#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc + y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - y;
    new_y = yc + x;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc + y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    new_x = xc - y;
    new_y = yc - x;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    //7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x=0;int y=R;
  int p=3-2*R;
  while (x<=y)
  {
   Draw8Points(xc,yc,x,y,ren);
   if(p<0){
        p=p+4*x+6;
   }
   else
       {
            p=p+4*(x-y)+10;
            y--;
       }
   x++;
  }
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x = 0; int y = R;
    int f = 1 - R;
    Draw8Points(xc, yc, x, y, ren);
    while (x < y)
    {
        if (f < 0) f += (x << 1) + 3;
        else
        {
            y--;
            f += ((x - y) << 1) + 5;
        }
        x++;
        Draw8Points(xc, yc, x, y, ren);
    }
}
