#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
   if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{

}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{

}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{

}
