#include <iostream>
#include <SDL2/SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Ellipse.h"
#include "Circle.h"
using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE
    Vector2D p1(600,200),p2(100,200),p3(500,500),p4(600,700);
    SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
    SDL_Color fillcolor;
    fillcolor.a=150;
    fillcolor.b=100;
    fillcolor.g=200;
    //TriangleFill(p1,p2,p3,ren,fillcolor);
    //DrawCurve2(ren,p1,p2,p3);
    RectangleFill(p4,p2,ren,fillcolor);
    //CircleFill(250,500,100,ren,fillcolor);
    //CircleFill(300,400,100,ren,fillcolor);
    //FillIntersectionTwoCircles(250,500,100,300,250,200,ren,fillcolor);
    MidpointDrawCircle(300,400,100,ren);
    MidPointDrawEllipse(250,500,100,200,ren);

    FillIntersectionEllipseCircle(250,500,100,200,300,400,100,ren,fillcolor);

    SDL_RenderPresent(ren);
    //Take a quick break after all that hard work
    //Quit if happen QUIT event

	bool running = true;
	int D3=5000,D1=5000,D2=5000,x,y;
	while(running)
	{
        //If there's events to handle
        if(SDL_PollEvent( &event))
        {

            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                running = false;
            }
            else{
			if (event.type == SDL_MOUSEBUTTONDOWN){
                x = event.button.x;
                y = event.button.y;
				D1 = (x - p1.x)*(x - p1.x) + (y - p1.y)*(y - p1.y);
				D3 = (x - p3.x)*(x - p3.x) + (y - p3.y)*(y - p3.y);
				D2 = (x - p2.x)*(x - p2.x) + (y - p2.y)*(y - p2.y);
			}
            if(event.type == SDL_MOUSEMOTION){
                if (D1 <= 50*50) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					p1.x = event.button.x;
					p1.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
					DrawCurve2(ren, p1, p2, p3);
					SDL_RenderPresent(ren);
                }
                else if (D3 <= 50*50) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					p3.x = event.button.x;
					p3.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
					DrawCurve2(ren, p1, p2, p3);
					SDL_RenderPresent(ren);
				}
				else if (D2 <= 50*50) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					p2.x = event.button.x;
					p2.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
					DrawCurve2(ren, p1, p2, p3);
					SDL_RenderPresent(ren);
				}
			}
            if(event.type==SDL_MOUSEBUTTONUP){
                D3=5000;
                D1=5000;
                D2=5000;
            }
        }
        }
    }

	SDL_DestroyRenderer(ren);
	//SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
