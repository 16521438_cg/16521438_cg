#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
    int x,y,a;
    x=0;
    y=0;
    a=(float)(1/(2*A));
    int p;
    p=-1;
    while(x<=A){
        Draw2Points(xc,yc,x,y,ren);
        if(p<0){
            p+=2*(a*(2*x+3));
        }
        else{
            p+=2*(a*(2*x+3))-1;
            y++;
        }
        x++;
    }
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{

}
