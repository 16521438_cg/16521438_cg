#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

     new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

     new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

     new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    float p,a2,b2,p2;
    int x,y;
    a2=a*a;
    b2=b*b;
    // Area 1
    x=0;
    y=b;
    p=2*((float)b2/a2)-(2*b)+1;
    //p2=((float)b2/a2)*x;
    while(((float)b2/a2)*x<=y){
        Draw4Points(xc,yc,x,y,ren);
        if(p<0)
            p=p+(2*(b2/a2)*(2*x+3));
        else{
            p=p+(2*(b2/a2)*(2*x+3))-4*y;
            y--;
        }
        x++;
    }
    // Area 2
    y=0;
    x=a;
    p=2*((float)a2/b2)-(2*a)+1;
    //p2=((float)x2/y2)*y;
    while(((float)a2/b2)*y<=x){
        Draw4Points(xc,yc,x,y,ren);
        if(p<0)
            p=p+(2*(a2/b2)*(2*y+3));
        else{
            p=p+(2*(a2/b2)*(2*y+3))-4*x;
            x--;
        }
        y++;
    }
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    int x,y;
    x=0;
    y=b;
    int p,a2,b2,q,t;
    a2=a*a; b2=b*b;
    p=b2-a2*b+a2>>2;
    t=2*a2*y;
    q=0;
    // Area 1
    while(q<t){
        q+=2*b2;
        if(p<0){
            p=p+b2*(2*x+3);
        }
        else{
            p=p+b2*(2*x+3)+a2*2*(1-y);
            t-=2*a2;
            y--;
        }
        x++;
        Draw4Points(xc,yc,x,y,ren);
    }
    p=b2*(x+0.5)*(x+0.5) +a2*(y-1)*(y-1)-a2*b2;

    // Area 2
    while(y>0)
    {
        t=t-2*a2;
        if(p>=0)
        {
            p=p+a2*(3-2*y);
        }
        else
        {
            p=p+b2*(2*x+2)+a2*(3-2*y);
            q=q+2*b2;
            x++;
        }
        Draw4Points(xc,yc,x,y,ren);
        y--;
    }

}
